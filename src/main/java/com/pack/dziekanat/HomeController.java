package com.pack.dziekanat;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class HomeController {
    @RequestMapping("/home")
    public String welcome(Model model){
        model.addAttribute("greeting", "Dziekanat");
        model.addAttribute("tagline", "test");
        return "welcome";
    }
}
